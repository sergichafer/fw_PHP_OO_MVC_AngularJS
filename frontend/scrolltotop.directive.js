watupper.directive('scrollToTop', function () {
    var directive = {
        scope: {},
        restrict: 'AE',
        template: "<div class=\"scroll-top-wrapper \">\n\
                        <span class=\"scroll-top-inner\">\n\
                            <i class=\"icon-arrow-up\"></i>\n\
                        </span>\n\
                   </div>",
        link: function () {

            $(function () {
                $(document).on('scroll', function () {
                    if ($(window).scrollTop() > 100) {
                        $('.scroll-top-wrapper').addClass('show');
                    } else {
                        $('.scroll-top-wrapper').removeClass('show');
                    }
                });
            });

            $(function () {
                $(document).on('scroll', function () {
                    if ($(window).scrollTop() > 100) {
                        $('.scroll-top-wrapper').addClass('show');
                    } else {
                        $('.scroll-top-wrapper').removeClass('show');
                    }
                });
                $('.scroll-top-wrapper').on('click', scrollToTop);
            });

            function scrollToTop() {
                verticalOffset = typeof (verticalOffset) !== 'undefined' ? verticalOffset : 0;
                element = $('body');
                offset = element.offset();
                offsetTop = offset.top;
                $('html, body').animate({scrollTop: offsetTop}, 350, 'linear');
            }
        }
    };
    return directive;
});