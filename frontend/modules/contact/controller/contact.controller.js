watupper.controller('contactCtrl', function ($rootScope, $scope, products, products_map, services, CommonService) {
    $scope.contact = {
        inputName: "",
        inputEmail: "",
        inputSubject: "",
        inputMessage: ""
    };
    // console.log(products.products);
    $scope.products = products.products;
    $scope.markers = [];
    $scope.mapholderV = true;

    products_map.cargarmap(products.products, $scope);

    $scope.SubmitContact = function () {
        var data = {"inputName": $scope.contact.inputName, "inputEmail": $scope.contact.inputEmail, 
        "inputSubject": $scope.contact.inputSubject, "inputMessage": $scope.contact.inputMessage,"token":'contact_form'};
        var contact_form = JSON.stringify(data);
        // console.log(contact_form);
        services.post('contact', 'process_contact', contact_form).then(function (response) {
            console.log(response);
            response = response.split("|");
            $scope.message = response[2];
            console.log(response[1]);
            if (response[1] == 'true') {
                // $scope.class = 'alert alert-success';
                CommonService.customToaster(response[2], '1000', 'top right', 'md-toast-done');
            } else {
                // $scope.class = 'alert alert-error';
                CommonService.customToaster(response[2], '1000', 'top right', 'md-toast-error');
            }
        });
    };
});