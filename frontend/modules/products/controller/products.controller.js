watupper.controller('productsCtrl', function ($scope, products, $route) {

    $scope.filteredProducts = [];
    $scope.product = products.products;
    $scope.numPerPage = 6;
    $scope.maxSize = 5;
    $scope.currentPage = 1;
    $scope.type = $route.current.params.type;
    $scope.titles = "";
    $scope.simulateQuery = false;
    $scope.isDisabled = false;
    $scope.querySearch = querySearch;
    $scope.selectedItemChange = this.selectedItemChange;
    $scope.searchTextChange = this.searchTextChange;
    var x=1;
    angular.forEach($scope.product, function(value, key){
    	if (x==$scope.product.length) {
    		$scope.titles=$scope.titles+value.title;
    	}else{
    		$scope.titles=$scope.titles+value.title+", ";
    		x++;
    	}
   	});
   	// $scope.titles=JSON.stringify($scope.titles);
   	// console.log($scope.titles);
   	$scope.titlesAvailables = loadAll();
    $scope.filteredProducts = $scope.product.slice(0, 3);
	


	$scope.pageChanged = function() {
	  var startPos = ($scope.currentPage - 1) * 3;
	  $scope.filteredProducts = $scope.product.slice(startPos, startPos + 3);
	  // console.log($scope.currentPage);
	};

	function querySearch (query) {
      var results = query ? $scope.titlesAvailables.filter( createFilterFor(query) ) : $scope.titlesAvailables,
          deferred;
      if ($scope.simulateQuery) {
        deferred = $q.defer();
        $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
        return deferred.promise;
      } else {
        return results;
      }
    }

	function loadAll() {
      var allTitles = $scope.titles;

      return allTitles.split(/, +/g).map( function (title) {
        return {
          value: title.toLowerCase(),
          display: title
        };
      });
    }

    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(title) {
        return (title.value.indexOf(lowercaseQuery) === 0);
      };

    };
});



watupper.controller('detailsCtrl', function ($rootScope, $scope, data, ratings, products_map, services, $timeout, $route, CommonService, cookiesService) {
    console.log(data);
    $scope.data = data.products;
    $scope.ratings = ratings.ratings;
    $scope.markers = [];
	  $scope.mapholderV = true;
    $scope.online = false;
    $scope.comment = "";
    $scope.max = 5;
    $scope.warning="Rate this tupper";
    $scope.isReadonly = true;

    setTimeout(function(){ products_map.cargarmapProduct($scope.data, $scope); }, 10);
    var data = {"tupper": $scope.data.reference};
    tupper = JSON.stringify(data);
    services.post('products', 'ratings', {avg_likes: tupper}).then(function (response) {
      console.log(response);
      if (response.success) {
        $scope.rate = response.result[0].avg;
        $scope.opinions = response.result[0].opinions;
      } else if (response.error) {
        $scope.rate = null;
      }
    });

    var user = cookiesService.GetCredentials();

    if (user) {
        var data = {"email": $rootScope.email_ratings, "tupper": $scope.data.reference};
        var like = JSON.stringify(data);
        services.post('products', 'ratings', {check_stars: like}).then(function (response) {
          // console.log(response);
            if (response.success) {
              $scope.isReadonly = false;
            } else if (response.error) {
              $scope.warning=response.result;
            }
        });

        $scope.post_rate = function() {
        // $scope.$watch('rate', function() {
            var data = {"email": $rootScope.email_ratings, "stars": $scope.rate, "tupper": $scope.data.reference};
            var stars = JSON.stringify(data);
            console.log(stars);
            services.post('products', 'ratings', {stars: stars}).then(function (response) {
              if (response.success) {
                  $timeout(function () {
                      $route.reload();
                      CommonService.customToaster("Rated successfully!.", '1000', 'top right', 'md-toast-done');
                  }, 2000);
              } else {
                $timeout(function () {
                      $route.reload();
                      CommonService.customToaster("Server error, try again later!", '1000', 'top right', 'md-toast-error');
                  }, 2000);    
              }
            });
        };

        $scope.enableSave = function(comment) {
          if (comment) {
            return comment.length > 1;
          }else{
            return false;
          }   
        };

        $scope.online = true;
        $scope.send_comment = function () {
          var data = {"email": $rootScope.email_ratings, "comment": $scope.comment, "tupper": $scope.data.reference};
          var comment = JSON.stringify(data);
          // console.log(comment);
          services.post('products', 'ratings', {comment: comment}).then(function (response) {
              // console.log(response);
              if (response.success) {
                  $timeout(function () {
                      $route.reload();
                      CommonService.customToaster("Posted! Don't mind on checking it.", '1000', 'top right', 'md-toast-done');
                  }, 2000);
              } else if (response.error) {
                $timeout(function () {
                      $route.reload();
                      CommonService.customToaster(response.result, '1000', 'top right', 'md-toast-error');
                  }, 2000);    
              }
          });
        }
    };

    // console.log($scope.ratings);
    // products_map.cargarmap(data.products, $scope);


});