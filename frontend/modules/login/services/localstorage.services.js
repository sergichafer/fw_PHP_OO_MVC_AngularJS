watupper.factory("localstorageService", ['$timeout', '$filter', '$q', function ($timeout, $filter, $q) { 
        var service = {};
        service.GetAll = GetAll;
        service.GetById = GetById;
        service.GetByToken = GetByToken;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        return service;

        function GetAll() {
            var deferred = $q.defer();
            deferred.resolve({ success: true, message: 'GetAll success', data: getUsers() });
            return deferred.promise;
        }

        function GetById(id) {
            var deferred = $q.defer();
            var filtered = $filter('filter')(getUsers(), { id: id });
            var token = filtered.length ? filtered[0] : null;
            if(token === null){
                deferred.resolve({ success: false, message: 'GetById error', data: null });
            }else{
                deferred.resolve({ success: true, message: 'GetById success', data: token });
            }
            return deferred.promise;
        }

        function GetByToken(token) {
            var deferred = $q.defer();
            var filtered = $filter('filter')(getUsers(), { token: token });
            var token = filtered.length ? filtered[0] : null;
            if(token === null){
                deferred.resolve({ success: false, message: 'GetByUsername error', data: null });
            }else{
                deferred.resolve({ success: true, message: 'GetByUsername success', data: token });
            }
            return deferred.promise;
        }

        function Create(token) {
            var deferred = $q.defer();
            $timeout(function () {
                GetByToken(token)
                    .then(function () {
                        var users = getUsers();
                        // assign id
                        var lastUser = users[users.length - 1] || { id: 0 };
                        token.id = lastUser.id + 1;

                        // save to local storage
                        users.push(token);
                        setUsers(users);
                        deferred.resolve({ success: true, message: 'User created success' });
                    });
            }, 1000);
            return deferred.promise;
        }

        function Update(token) {
            var deferred = $q.defer();
            var users = getUsers();
            for (var i = 0; i < users.length; i++) {
                if (users[i].id === token.id) {
                    users[i] = token;
                    break;
                }
            }
            setUsers(users);
            //deferred.resolve();
            deferred.resolve({ success: true, message: 'User updated success' });
            return deferred.promise;
        }
        
        function Delete(id) {
            var deferred = $q.defer();
            var users = getUsers();
            for (var i = 0; i < users.length; i++) {
                var token = users[i];
                if (token.id === id) {
                    users.splice(i, 1);
                    break;
                }
            }
            setUsers(users);
            deferred.resolve({ success: true, message: 'User deleted success' });
            return deferred.promise;
        }

        // private functions
        function getUsers() {
            if(!localStorage.users){
                localStorage.users = JSON.stringify([]);
            }
            return JSON.parse(localStorage.users);
        }
        
        function setUsers(users) {
            localStorage.users = JSON.stringify(users);
        }
}]);
