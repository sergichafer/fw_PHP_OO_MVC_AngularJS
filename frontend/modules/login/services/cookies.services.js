watupper.factory("cookiesService", ['$cookies', 'localstorageService',
    function ($cookies, localstorageService) {
        var service = {};
        service.SetCredentials = SetCredentials;
        service.ClearCredentials = ClearCredentials;
        service.GetCredentials = GetCredentials;
        return service;

        function SetCredentials(token) {
            
            //almacenarlos en la cookie session
            $cookies.putObject("session", 
            {token: token}, 
            {expires: new Date(new Date().getTime() + 24 * 60 * 60 * 1000)});
            
            //almacenarlos en localstorage
            localstorageService.Create(token).then(function (response) {
                //console.log(response.success);
                if (response.success) {
                    console.log(response.message);
                } else {
                    console.log(response.message);
                }
            });
        }

        function ClearCredentials() {
            $cookies.remove("session");
        }
        
        function GetCredentials() {
            //al cargarse la pagina por primera vez, user es undefined
            var token = $cookies.getObject("session");
            return token;
        }
        
    }]);