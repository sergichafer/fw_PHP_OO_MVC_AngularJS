watupper.factory("social_signin", ['$location', '$rootScope', 'services', 'cookiesService', 'initService', '$mdDialog',
function ($location, $rootScope, services, cookiesService, initService, $mdDialog) {
	var service = {};
        service.google_login = google_login;
        service.github_login = github_login;
        service.twitter_login = twitter_login;
    return service;
    function initializeApp() {
		var config = {
			apiKey: "AIzaSyARWDQ5EbIEB4CpfPD9g6yT9tI4FeLkicM",
			authDomain: "watupper-85be0.firebaseapp.com",
			databaseURL: "https://watupper-85be0.firebaseio.com",
			projectId: "watupper-85be0",
			storageBucket: "watupper-85be0.appspot.com",
			messagingSenderId: "946012518323"
		};

		firebase.initializeApp(config);
	}

	function google_login() {
		initializeApp();
		var provider = new firebase.auth.GoogleAuthProvider();

	    var authService = firebase.auth();

	    // manejador de eventos para loguearse
	    
		authService.signInWithPopup(provider)
			.then(function(result) {
			    // console.log(result.user.uid);
			    // console.log(result.credential.accessToken);
			    var data = {"token":result.user.uid, "email": result.user.email, "avatar": result.user.photoURL};
				var login_JSON = JSON.stringify(data);
				social_login(login_JSON);
		})
	}

	function github_login() {
		var provider = new firebase.auth.GithubAuthProvider();

		var authService = firebase.auth();

		authService.signInWithPopup(provider)
			.then(function(result) {
			    //var token = result.credential.accessToken;
			    var data = {"token":result.user.uid, "email": result.user.email, "avatar": result.user.photoURL};
		    	var login_JSON = JSON.stringify(data);
		    	social_login(login_JSON);
		    	// console.log(login_JSON);
		})
	}

	function twitter_login() {
		var provider = new firebase.auth.TwitterAuthProvider();
    	var authService = firebase.auth();

		authService.signInWithPopup(provider)
			.then(function(result) {
			    // console.log(result.credential.accessToken);
			    var data = {"token":result.user.uid, "email": result.user.displayName, "avatar": result.user.photoURL};
		    	var login_JSON = JSON.stringify(data);
		    	social_login(login_JSON);
		    	// console.log(login_JSON);
		})
	}

	function social_login(login_JSON) {
		services.post('login', 'social_signin', {user: login_JSON})
            .then(function (response) {
                // console.log(response);
                cookiesService.SetCredentials(response[0]);
                $mdDialog.cancel();
                initService.login();
                $location.path("/");
        });
	}
}]);