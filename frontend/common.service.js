watupper.factory("CommonService", ['$rootScope','$timeout','$mdToast', function ($rootScope, $timeout, $mdToast) {
        var service = {};
        service.banner = banner;
        service.amigable = amigable;
        service.customToaster = customToaster;
        service.defaultToaster = defaultToaster;
        return service;

        function banner(message, type) {
            $rootScope.bannerText = message;
            $rootScope.bannerClass = 'alertbanner aletbanner' + type;
            $rootScope.bannerV = true;

            $timeout(function () {
                $rootScope.bannerV = false;
                $rootScope.bannerText = "";
            }, 5000);
        }
        
        function amigable(url) {
            var link = "";
            url = url.replace("?", "");
            url = url.split("&");

            for (var i = 0; i < url.length; i++) {
                var aux = url[i].split("=");
                link += aux[1] + "/";
            }
            return link;
        }
        
        function customToaster(content , delay, position, type) {
            $mdToast.show(
              $mdToast.simple()
                .toastClass(type)
                .textContent(content)
                .position(position)
                .hideDelay(delay)
            );
        }

        function defaultToaster(content) {
            $mdToast.show(
              $mdToast.simple()
                .textContent(content)
                .position('top right')
                .hideDelay(3000)
            );
        }

    }]);
