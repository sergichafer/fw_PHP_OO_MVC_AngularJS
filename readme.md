# Watupper, a FW_PHP_OO_MVC_AngularJS educational purposes project

## Introduction

This is a basic app that offers login options(register, recover, restore, validations against DAO's, social login alternatives, profile and dependent menus), a listing based on the database made for the app and some contact options for the client.

## Code Samples

To continue with the project up there explained, I'll proceed with the remarkable code inductions.

This project contains code improvements which can be listed here:

-Register validation, functions to acomplish a proper register and proceed with the process keeping in mind a few control errors.

-Login through database option using the JWT with data inside encrypted using MD5 process or social alternatives with the same performance.
    Each user has his own profile page where the user itself can edit personal data and also a personal menu is displayed until the logout button is pressed.

-Website main page is adaptable depending on the most searched products, this ones are displayed in his own page but listed at the main page front for a better relevance, taking care on each user interactions.

-Each product is related to a store and its location is listed depending on the user current location, both in the contact page and product individual page.

-Date service to avoid a few repetitive codelines.

-APP.js using .run to avoid a few bugs related to login issues.

-$rootscope used for global var's, functions or even text.

-Social login using firebase with his own .services.js .

-Scroll to top directive used in the whole app.

-Angular.js material used in most of the website views to avoid using jquery and achieve better performance.

-Different listing options for the profucts were added since the last version.

-Added rating system with a special dedication on respect and 0 failure, it can be appreciated for each product.
It uses angularjs bootstrap UI, a restricting directive and a ton of DAO validations.

There are more code highlights or details but not so remarkable, will be better appreciated at the web app code.

## Installation

> Finally, the way to use the web app is to execute the SQL script found in the DB folder and run the code in an apache server.

## Template site:

TITLE: 
Shop - 100% Fully Responsive Free HTML5 Bootstrap Template

AUTHOR:
DESIGNED & DEVELOPED by FreeHTML5.co

Website: http://freehtml5.co/
Twitter: http://twitter.com/fh5co
Facebook: http://facebook.com/fh5co