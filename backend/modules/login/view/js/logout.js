$(document).ready(function () {
    $("a#logout").click(function () {
        logout();
    });
});

function logout(){
    Tools.eraseCookie("token");
    window.location.href = amigable("?module=main");
}