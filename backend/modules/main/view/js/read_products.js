$(document).ready(function () {
var value=window.location.pathname.replace(/%/g, "").replace(/20/g, " ").split('/')[4];
// console.log(value);
search_product(value);
});

function search_product(keyword) {
    $.post(amigable("?module=main&function=obtain_products"), {'increment': true,'keyword': keyword}, function (data, status) {
        var message = JSON.parse(data);
        reset();
        if(message){
               $.each(message, function(index, object) {
                    var $row = $('<center><div class="col-md-12">'
                    + '<h3 class="title"><b> '+object.title+'</b></h3>'
                    + '<hr />'
                    + '<img src="'+object.img+'"></img>'
                    + '</div>'
                    + '<br /><br />'
                    + '<div class="col-md-12 product_content">'
                    + '<hr />'
                    + '</center>'
                    + '<p>'
                    + '<h5><em>#'+object.reference+'</em></h5>'
                    + '</p>'
                    + '<div class="rating">'
                    + '<span class="glyphicon glyphicon-star"></span>'
                    + '<span class="glyphicon glyphicon-star"></span>   '
                    + '<span class="glyphicon glyphicon-star"></span>'
                    + '<span class="glyphicon glyphicon-star"></span>'
                    + '<span class="glyphicon glyphicon-star"></span>'
                    + '</div>'
                    + '<p>'
                    + '<h4 class="category"><b>Type:</b> '+object.alimentation+'</h4>'
                    + '</p>'
                    + '<p>'
                    + '<h4 class="category">'+object.details+'</h4>'
                    + '<br />'
                    + '<div>'
                    + '<h2 class="category"><span class="glyphicon glyphicon-tags"></span>&nbsp;<b>'+object.price+'€</b></h2>'
                    + '</div>'
                    + '<br />'
                    + '</div>'
                    + '</div>'
                    + '<br />'
                    + '<div id="map"></div>'
                    + '<br />'
                    + '<div class="btn-ground">'
                    + '<a class="btn btn-default" href="../../main/all_products/"><span class="glyphicon glyphicon-log-in"></span>&nbsp;Return</a>'
                    + '</div>').appendTo("div#results");
                    $.post(amigable("?module=contact&function=list_store"),{'store': object.store}, function (data) {
                        var map = new google.maps.Map(document.getElementById('map'), {
                            center: {lat: -34.397, lng: 150.644},
                            zoom: 6
                          });
                          var infoWindow = new google.maps.InfoWindow({map: map});

                          // Try HTML5 geolocation.
                          if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                              var pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                              };

                              infoWindow.setPosition(pos);
                              infoWindow.setContent('You');
                              map.setCenter(pos);
                            }, function() {
                              handleLocationError(true, infoWindow, map.getCenter());
                            });
                          } else {
                            // Browser doesn't support Geolocation
                            handleLocationError(false, infoWindow, map.getCenter());
                          }
                        // console.log(data);
                        var message = JSON.parse(data);
                        // console.log(message);
                        $.each(message, function(index, object) {
                          var contentString = '<div class="inner"">'+
                              '<div id="siteNotice">'+
                              '</div>'+
                              '<h1 id="firstHeading" class="firstHeading">'+object.storename+'</h1>'+
                              '<div id="bodyContent">'+
                              '<p><b>'+object.storename+'</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum sodales nisl nec volutpat mollis. Cras finibus cursus ultrices. Curabitur egestas.</p>'+
                              '</div>'+
                              '</div>';

                          var infowindow = new google.maps.InfoWindow({
                            content: contentString
                          });

                          var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(object.lat, object.lng),
                            map: map,
                            title: object.storename
                          });
                          marker.addListener('click', function() {
                            infowindow.open(map, marker);
                          });
                        })
                      });

                })
            }else if(message==1){  
                $("div#results").html('Wrong data!');

            }else{
                $("div#results").html('Unexpected error...');
            };
    }).fail(function (xhr) {
        $("div#results").html('Unexpected error...');
        reset();
    });
}

function reset() {
    var $div = $("div#results");
    $div.html("");
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' : 'Error: Your browser doesn\'t support geolocation.');
}