<?php
class products_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_product_DAO($db, $arrArgument) {
        $reference = $arrArgument['reference'];
        $title = $arrArgument['title'];
        $inidate = $arrArgument['inidate'];
        $finidate = $arrArgument['finidate'];
        $interval = $arrArgument['interval'];
        $array = $arrArgument['allergens'];
        foreach ($array as &$value) {
              $allergens=$value.":";
          }

        $alimentation = $arrArgument['alimentation'];
        $specific = $arrArgument['specific'];
        $nutrients = $arrArgument['nutrients'];
        $details = utf8_decode($arrArgument['details']);
        $price = $arrArgument['price'];
        $img = $arrArgument['prodpic'];
        
        $sql = "INSERT INTO `tuppers` (`reference`, `title`, `inidate`, `finidate`,"
                . " `interval`, `allergens`, `alimentation`, `specific`, `nutrients`,"
                . " `details`, `price`, `img`, `relevancy`) VALUES ('$reference', '$title',"
                . " '$inidate', '$finidate', '$interval', '$allergens', "
                . " '$alimentation', '$specific', '$nutrients',"
                . " '$details', '$price', '$img', '0')";

        return $db->ejecutar($sql);
    }
    public function list_products_DAO($db) {
        $sql = "SELECT * FROM tuppers";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function delete_product_DAO($db, $criteria) {
        $sql = "DELETE FROM tuppers WHERE reference LIKE '" . $criteria . "'";
        return $db->ejecutar($sql);
    }

    public function checkcomment_DAO($db, $criteria) {
        $email = $criteria['email'];
        $tupper = $criteria['tupper'];
        $sql = "SELECT comment FROM ratings WHERE email LIKE '" . $email . "' AND reference LIKE '" . $tupper . "' AND comment IS NOT NULL";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function checklike_DAO($db, $criteria) {
        $email = $criteria['email'];
        $tupper = $criteria['tupper'];
        $sql = "SELECT stars FROM ratings WHERE email LIKE '" . $email . "' AND reference LIKE '" . $tupper . "'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function check_created_DAO($db, $criteria) {
        $email = $criteria['email'];
        $tupper = $criteria['tupper'];
        $sql = "SELECT * FROM ratings WHERE email LIKE '" . $email . "' AND reference LIKE '" . $tupper . "'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function update_rating_DAO($db, $criteria) {
        $email = $criteria['email'];
        $tupper = $criteria['tupper'];
        if($criteria['comment']) {
            $comment=$criteria['comment'];
            $sql = "UPDATE ratings SET comment='" . $comment . "' WHERE email LIKE '" . $email . "' AND reference LIKE '" . $tupper . "'";
        } else if($criteria['stars']) {
            $stars=$criteria['stars'];
            $sql = "UPDATE ratings SET stars='" . $stars . "' WHERE email LIKE '" . $email . "' AND reference LIKE '" . $tupper . "'";
        }
        return $db->ejecutar($sql);
    }

    public function post_comment_DAO($db, $criteria) {
        $email = $criteria['email'];
        $tupper = $criteria['tupper'];
        $comment = $criteria['comment'];
        $sql = "INSERT INTO ratings (`email`, `reference`, `comment`, `issuedat`) 
                VALUES ('" . $email . "' ,'" . $tupper . "' , '" . $comment . "', now())";
        return $db->ejecutar($sql);
    }

    public function avg_likes_DAO($db, $reference) {
        $criteria=$reference['tupper'];
        $sql = "SELECT ROUND(AVG(stars), 1) AS avg, COUNT(stars) AS opinions FROM ratings WHERE reference LIKE '" . $criteria . "'AND stars IS NOT NULL";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function check_stars_DAO($db, $criteria) {
        $email = $criteria['email'];
        $tupper = $criteria['tupper'];
        $sql = "SELECT stars FROM ratings WHERE email LIKE '" . $email . "' AND reference LIKE '" . $tupper . "' AND stars IS NOT NULL";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function post_stars_DAO($db, $criteria) {
        $email = $criteria['email'];
        $tupper = $criteria['tupper'];
        $stars = $criteria['stars'];
        $sql = "INSERT INTO ratings (`email`, `reference`, `stars`, `issuedat`) 
                VALUES ('" . $email . "' ,'" . $tupper . "' , '" . $stars . "', now())";
        return $db->ejecutar($sql);
    }

}//End productDAO
