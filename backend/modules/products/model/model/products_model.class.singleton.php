<?php
class products_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = products_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_product($arrArgument) {
        return $this->bll->create_product_BLL($arrArgument);
    }

    public function list_products() {
        return $this->bll->list_products_BLL();
    }

    public function delete_product($criteria) {
        return $this->bll->delete_product_BLL($criteria);
    }

    public function checkcomment($criteria) {
        return $this->bll->checkcomment_BLL($criteria);
    }

    public function check_created($criteria) {
        return $this->bll->check_created_BLL($criteria);
    }

    public function update_rating($criteria) {
        return $this->bll->update_rating_BLL($criteria);
    }

    public function post_comment($criteria) {
        return $this->bll->post_comment_BLL($criteria);
    }

    public function avg_likes($reference) {
        return $this->bll->avg_likes_BLL($reference);
    }

    public function check_stars($criteria) {
        return $this->bll->check_stars_BLL($criteria);
    }

    public function post_stars($criteria) {
        return $this->bll->post_stars_BLL($criteria);
    }

}
