////////////////////////////////////////////////////////////////
function load_products() {
    $.ajax({
        type: 'POST',
        url: amigable("?module=products&function=load_products"),
        data: {'load':true},
    }).done(function (data) {
        var json = JSON.parse(data);
        //alert(json.user.usuario);

        print_product(json);

    }).fail(function (xhr) {
        alert(xhr.responseText);
    });
}

$(document).ready(function () {
    load_products();
});

function print_product(data) {
    //alert(data.user.avatar);
    console.log(data);
    //console.log(data.product);
    var content = document.getElementById("content");
    var div_product = document.createElement("div");
    var parrafo = document.createElement("p");

    var message = document.createElement("div");
    message.innerHTML = "message = ";
    message.innerHTML += data.message;

    var reference = document.createElement("div");
    reference.innerHTML = "reference = ";
    reference.innerHTML += data.product.reference;

    var title = document.createElement("div");
    title.innerHTML = "title = ";
    title.innerHTML += data.product.title;

    var inidate = document.createElement("div");
    inidate.innerHTML = "inidate = ";
    inidate.innerHTML += data.product.inidate;

    var finidate = document.createElement("div");
    finidate.innerHTML = "finidate = ";
    finidate.innerHTML += data.product.finidate;

    var interval = document.createElement("div");
    interval.innerHTML = "interval = ";
    interval.innerHTML += data.product.interval;

    var allergens = document.createElement("div");
    allergens.innerHTML = "allergens = ";
    for(var i =0;i < data.product.allergens.length;i++){
    allergens.innerHTML += data.product.allergens[i]+":";
    }

    var alimentation = document.createElement("div");
    alimentation.innerHTML = "alimentation = ";
    alimentation.innerHTML += data.product.alimentation;

    var specific = document.createElement("div");
    specific.innerHTML = "specific = ";
    specific.innerHTML += data.product.specific;

    var nutrients = document.createElement("div");
    nutrients.innerHTML = "nutrients = ";
    nutrients.innerHTML += data.product.nutrients;

    var details = document.createElement("div");
    details.innerHTML = "details = ";
    details.innerHTML += data.product.details;

    var price = document.createElement("div");
    price.innerHTML = "price = ";
    price.innerHTML += data.product.price;

    //arreglar ruta IMATGE!!!!!

    var cad = data.product.prodpic;
    //console.log(cad);
    //var cad = cad.toLowerCase();
    var img = document.createElement("div");
    var html = '<img src="' + cad + '" height="100" width="100"> ';
    img.innerHTML = html;
    //alert(html);

    div_product.appendChild(parrafo);
    parrafo.appendChild(message);
    parrafo.appendChild(reference);
    parrafo.appendChild(title);
    parrafo.appendChild(inidate);
    parrafo.appendChild(finidate);
    parrafo.appendChild(interval);
    parrafo.appendChild(allergens);
    parrafo.appendChild(alimentation);
    parrafo.appendChild(specific);
    parrafo.appendChild(nutrients);
    parrafo.appendChild(details);
    parrafo.appendChild(price);
    content.appendChild(div_product);
    content.appendChild(img);
}
