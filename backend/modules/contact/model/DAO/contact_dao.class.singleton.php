<?php
class contact_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function list_stores_dao($db) {
        $sql = "SELECT * FROM stores";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);

    }

}