<?php
class contact_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = contact_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function list_stores(){
        return $this->bll->list_stores_bll();
    }

}
